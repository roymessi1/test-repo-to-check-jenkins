class BuildEnv:
    def onBuild(self, content):
        content = content.replace("<!--PROD_HEAD_EXT-->",'').replace("<!--PROD_TOB_BODY_EXT-->",'').replace("<!--PROD_BOTTOM_BODY_EXT-->",'')
        content = content.replace("<!--DEV_HEAD_EXT-->",'').replace("<!--DEV_TOB_BODY_EXT-->",'').replace("<!--DEV_BOTTOM_BODY_EXT-->",'')
        return content