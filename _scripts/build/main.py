import sys, os, glob, re, json, htmlmin, jsmin, codecs
import build_dev, build_prod
from urllib import urlopen
from csscompressor import compress

## Global Variables
LANGUAGES = ["en", "ar", "fr", "it", "pt", "de", "es"]
VALID_GIT_ENVIRONMENTS = ['developer','prod']

## Make sure we get the git environment name
try:
    SCRIPT_NAME, GIT_ENVIRONMENT_NAME = sys.argv
except:
    sys.exit("ERROR. The git argument is missing.")

## Make sure we got a valid env name
if(GIT_ENVIRONMENT_NAME not in VALID_GIT_ENVIRONMENTS):
    print("ERROR. The git argument `{}` is not valid.\nplease pass one of those: {}".format(GIT_ENVIRONMENT_NAME,VALID_GIT_ENVIRONMENTS))
    sys.exit()

## Set env helper
if(GIT_ENVIRONMENT_NAME=='developer'):
    buildEnv = build_dev.BuildEnv()
else:
    buildEnv = build_prod.BuildEnv()

## Methods
def createLanguageEnvironment(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

def writeToFile(filePath, content):
    with codecs.open(filePath, 'w', 'utf-8') as dst_file:
        dst_file.write(content)

def minifyJSFiles():
    rootDir = './js'
    for subdir, dirs, files in os.walk(rootDir):
        for file in files:
            filePath = os.path.join(subdir, file)
            with codecs.open(filePath, 'r', 'utf-8') as src_file:
                writeToFile(filePath, jsmin.jsmin(src_file.read()))

def minifyCssFiles():
    rootDir = './css'
    for subdir, dirs, files in os.walk(rootDir):
        for file in files:
            filePath = os.path.join(subdir, file)
            with codecs.open(filePath, 'r', 'utf-8') as src_file:
                writeToFile(filePath, compress(src_file.read()))

class HandleHtmlFile:
    _dictionary = []
    _language = 'en'
    _filePath = '/'
    _fileContent = ''

    def __init__(self, language, fileName):
        self._language = language
        self._setDictionary()
        self._setFilePath(fileName)
        self._setFileContent(fileName)
        self._replacePlaceholders()
        self._extraManipulations()
        #
        self._writeToFile()

    def getFileContent(self):
        return self._fileContent

    def _writeToFile(self):
#         print(self._filePath, self._fileContent)
        writeToFile(self._filePath, self._fileContent)

    def _setFileContent(self, fileName):
        self._fileContent = (open('./'+fileName, 'r')).read()
#         self._fileContent = (open(self._filePath, "r")).read()
#         with codecs.open(self.filePath+fileName, 'r', 'utf-8') as fileContent:
#             self.fileContent = fileContent

    def _replacePlaceholders(self):
        placeholders = re.findall(r'{{\w+}}', self._fileContent)
        for placeholder in placeholders:
            key = placeholder.replace("{{","").replace("}}","")
            self._fileContent = self._fileContent.replace(placeholder,self._dictionary[key])

    def _extraManipulations(self):
        self._fileContent = buildEnv.onBuild(self._fileContent)
        # Minify the HTML
        self._fileContent = htmlmin.minify(unicode(self._fileContent).encode('utf8').decode("utf-8"), remove_empty_space=True)

    def _setDictionary(self):
        resp = urlopen('https://api.marketarena.com/dictionary/'+ self._language +'.json')
#         data = json.loads(unicode(resp.read()).encode('utf8').decode("utf-8"))
        data = json.loads(resp.read())

        data['dictionary']['app_Arabic'] = data['dictionary']['app_arabic']
        self._dictionary = data['dictionary']

    def _setFilePath(self,fileName):
        self._filePath = "./languages/{}/{}".format(self._language,fileName)

def main():
    for language in LANGUAGES:
        createLanguageEnvironment('languages/'+language)
        for fileName in glob.glob("*.html"):
            HandleHtmlFile(language, fileName)

    minifyJSFiles()
    minifyCssFiles()

if __name__ == '__main__':
    main()
